/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * This code was generated by https://code.google.com/p/google-apis-client-generator/
 * (build: 2013-05-23 17:46:09 UTC)
 * on 2013-05-27 at 17:37:45 UTC 
 * Modify at your own risk.
 */

package com.google.api.services.carmenzon.model;

/**
 * Model definition for ProdottoOrdinato.
 *
 * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
 * transmitted over HTTP when working with the . For a detailed explanation see:
 * <a href="http://code.google.com/p/google-http-java-client/wiki/JSON">http://code.google.com/p/google-http-java-client/wiki/JSON</a>
 * </p>
 *
 * @author Google, Inc.
 */
@SuppressWarnings("javadoc")
public final class ProdottoOrdinato extends com.google.api.client.json.GenericJson {

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.String aggiunta;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.String prodotto;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.Long quantita;

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getAggiunta() {
    return aggiunta;
  }

  /**
   * @param aggiunta aggiunta or {@code null} for none
   */
  public ProdottoOrdinato setAggiunta(java.lang.String aggiunta) {
    this.aggiunta = aggiunta;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getProdotto() {
    return prodotto;
  }

  /**
   * @param prodotto prodotto or {@code null} for none
   */
  public ProdottoOrdinato setProdotto(java.lang.String prodotto) {
    this.prodotto = prodotto;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.Long getQuantita() {
    return quantita;
  }

  /**
   * @param quantita quantita or {@code null} for none
   */
  public ProdottoOrdinato setQuantita(java.lang.Long quantita) {
    this.quantita = quantita;
    return this;
  }

  @Override
  public ProdottoOrdinato set(String fieldName, Object value) {
    return (ProdottoOrdinato) super.set(fieldName, value);
  }

  @Override
  public ProdottoOrdinato clone() {
    return (ProdottoOrdinato) super.clone();
  }

}
