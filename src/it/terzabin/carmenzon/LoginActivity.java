package it.terzabin.carmenzon;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.plus.PlusClient;
import com.google.api.services.carmenzon.CarmenzonScopes;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener, ConnectionCallbacks, OnConnectionFailedListener {

	static final int REQUEST_GOOGLE_PLAY_SERVICES = 0;
	static final int REQUEST_CODE_RESOLVE_ERR = 9000;

	private PlusClient plusClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		plusClient = new PlusClient.Builder(this, this, this)
		.setScopes(CarmenzonScopes.USERINFO_EMAIL, CarmenzonScopes.USERINFO_PROFILE)
		.build();

		findViewById(R.id.sign_in_button).setOnClickListener(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		plusClient.disconnect();

		super.onStop();
	}

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}*/

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.sign_in_button && !plusClient.isConnected()) {
			if (checkGooglePlayServicesAvailable()) {
				plusClient.connect();
			}
		}
	}

	private boolean checkGooglePlayServicesAvailable() {
		final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
			showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
			return false;
		}
		return true;
	}
	void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
		runOnUiThread(new Runnable() {
			public void run() {
				Dialog dialog = GooglePlayServicesUtil.getErrorDialog(connectionStatusCode, LoginActivity.this, REQUEST_GOOGLE_PLAY_SERVICES);
				dialog.show();
			}
		});
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		if (result.hasResolution()) {
			try {
				result.startResolutionForResult(this, REQUEST_CODE_RESOLVE_ERR);
			} catch (SendIntentException e) {
				plusClient.connect();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
		super.onActivityResult(requestCode, responseCode, intent);
		
		switch(requestCode) {
		case REQUEST_CODE_RESOLVE_ERR:
			if (responseCode == RESULT_OK) {
				plusClient.connect();
			}
		case REQUEST_GOOGLE_PLAY_SERVICES:
			if (responseCode == RESULT_OK) {
				plusClient.connect();
			} else {
				checkGooglePlayServicesAvailable();
			}
		}
	}

	@Override
	public void onConnected() {
		String accountName = plusClient.getAccountName();

		Toast.makeText(this, "Benvenuto, " + accountName + ".", Toast.LENGTH_LONG).show();

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("accountName", accountName);
		editor.commit();

		Intent returnIntent = new Intent();
		setResult(RESULT_OK, returnIntent);
		finish();
	}

	@Override
	public void onDisconnected() {

	}
}
