package it.terzabin.carmenzon;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import com.google.api.services.carmenzon.model.Prodotto;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class GrigliaProdottiAdapter extends ArrayAdapter<Entry<Prodotto, Long>> {

	public GrigliaProdottiAdapter(Context context, Map<Prodotto, Long> ordine) {
		super(context, 0,
			  new ArrayList<Entry<Prodotto, Long>>(ordine.entrySet()));
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {    
		ProdottoGridItemView view;    

		if(convertView != null && convertView instanceof ProdottoGridItemView) {
			view = (ProdottoGridItemView) convertView;
		} else {
			view = new ProdottoGridItemView(getContext());
		}          
		
		Entry<Prodotto, Long> entry = getItem(position);
		view.update(entry.getKey(), entry.getValue());

		return view;
	}
	
}
