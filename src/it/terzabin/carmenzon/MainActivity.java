package it.terzabin.carmenzon;

import java.io.IOException;
import java.io.Serializable;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.carmenzon.model.Prodotto;
import com.google.api.services.carmenzon.model.ProdottoCollection;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class MainActivity extends BaseActivity implements RequestListener<ProdottoCollection> {
	
	private static final String PRODOTTI = "prodotti";
	
	private ProdottoCollection prodotti;

	private LinearLayout loading_layout;  
	private RelativeLayout main_layout; 

	private ListaProdottiAdapter adapter;
	private ListView listView;
	private TextView totaleTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		loading_layout = (LinearLayout) findViewById(R.id.loading_layout);
		main_layout = (RelativeLayout) findViewById(R.id.main_layout);

		Button azzera = (Button) findViewById(R.id.button_azzera);

		totaleTextView = (TextView) findViewById(R.id.textView_valore_totale);
		listView = (ListView)findViewById(R.id.listView_panini);

		azzera.setOnClickListener(new View.OnClickListener(){
			@Override
			public void onClick(View v) {        		
				adapter.azzeraOrdine();
			}
		});
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		if (prodotti != null) {
			outState.putString(PRODOTTI, prodotti.toString());
		}
		
		//TODO: salvare anche l'ordine!
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		String prodottiJson = savedInstanceState.getString(PRODOTTI);
		if (prodottiJson != null) {
			try {
				prodotti = new GsonFactory().fromString(prodottiJson, ProdottoCollection.class);
				aggiornaView();
			} catch (IOException e) { }
		}
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_ordina:
			Intent intent = new Intent(MainActivity.this, ConfermaOrdineActivity.class);

			Map<Prodotto, Long> ordine = adapter.getOrdine();
			Map<String, Long> ordineJson = new HashMap<String, Long>();

			for (Entry<Prodotto, Long> entry : ordine.entrySet()) {
				String prodottoJson = entry.getKey().toString();

				ordineJson.put(prodottoJson, entry.getValue());
			}

			intent.putExtra(ConfermaOrdineActivity.ORDINE, (Serializable)ordineJson);

			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	protected void onReady() {
		ricaricaLista(false);
	}

	private void ricaricaLista(Boolean mostraLoading){
		if (mostraLoading) {
			loading_layout.setVisibility(View.VISIBLE);
			main_layout.setVisibility(View.GONE);
		}

		try {
			List prodotto = service.prodotti().list();
			prodotto.setDisponibile(true);
			EndpointsRequest<ProdottoCollection> request = new EndpointsRequest<ProdottoCollection>(prodotto);
			serviceManager.execute(request, request.cacheKey(), DurationInMillis.ONE_HOUR, this);
		} catch (IOException e) {
			mostraErrore(e);
		}

	}

	private void aggiornaView() {
		adapter = new ListaProdottiAdapter(getApplicationContext(), prodotti);
		adapter.registerDataSetObserver(new DataSetObserver() {
			@Override
			public void onChanged() {
				super.onChanged();

				aggiornaTotale();
			}
		});

		listView.setAdapter(adapter);
		aggiornaTotale();

		loading_layout.setVisibility(View.GONE);
		main_layout.setVisibility(View.VISIBLE);
	}

	protected void aggiornaTotale() {
		Map<Prodotto, Long> ordine = adapter.getOrdine();
		Double totale = 0.0;

		for (Entry<Prodotto, Long> entry : ordine.entrySet()) {
			Double prezzo = entry.getKey().getPrezzo();

			totale += prezzo * entry.getValue();
		}

		String totaleStr = NumberFormat.getCurrencyInstance(Locale.ITALY).format(totale);
		totaleTextView.setText(totaleStr);
	}

	private void mostraErrore(Exception exception) {
		Toast.makeText(MainActivity.this,
				"Errore: " + exception.getMessage(), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onRequestFailure(SpiceException spiceException) {
		mostraErrore(spiceException);
	}

	@Override
	public void onRequestSuccess(ProdottoCollection result) {
		prodotti = result;

		aggiornaView();
	}
}
