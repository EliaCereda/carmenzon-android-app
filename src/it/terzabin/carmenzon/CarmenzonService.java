package it.terzabin.carmenzon;

import android.app.Application;

import com.octo.android.robospice.SpiceService;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.googlehttpclient.json.GsonObjectPersisterFactory;

public class CarmenzonService extends SpiceService {

	@Override
	public CacheManager createCacheManager(Application application) {
		CacheManager cacheManager = new CacheManager();

		GsonObjectPersisterFactory gsonObjectPersisterFactory = new GsonObjectPersisterFactory(application);
		gsonObjectPersisterFactory.setAsyncSaveEnabled(true);
		cacheManager.addPersister(gsonObjectPersisterFactory);
		
		return cacheManager;
	}

}
