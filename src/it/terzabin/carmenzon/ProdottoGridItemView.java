package it.terzabin.carmenzon;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.api.services.carmenzon.model.Prodotto;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class ProdottoGridItemView extends RelativeLayout {
	
	private Prodotto prodottoCorrente;
	
	private ImageView imageView;
	private TextView nomeTextView;
	private TextView quantitaTextView;
	
	public ProdottoGridItemView(Context context) {
		super(context);
		
		inflateView(context);
	}
	
	private void inflateView(Context context) {
		LayoutInflater.from(context).inflate(R.layout.prodotto_grid_item, this);
		
		imageView = (ImageView) findViewById(R.id.imageView);
		nomeTextView = (TextView) findViewById(R.id.nomeTextView);
		quantitaTextView = (TextView) findViewById(R.id.quantitaTextView);
	}
	
	public void update(Prodotto prodotto, Long quantita) {
		if (prodottoCorrente != prodotto) {
			prodottoCorrente = prodotto;
			
			nomeTextView.setText(prodotto.getNome());
			quantitaTextView.setText("x" + quantita.toString());
			
			UrlImageViewHelper.setUrlDrawable(imageView, prodotto.getUrlImmagine(), R.drawable.prodotto_default, UrlImageViewHelper.CACHE_DURATION_INFINITE);
		}
		
		if (quantita == null)
			quantita = 0L;
	}
}
