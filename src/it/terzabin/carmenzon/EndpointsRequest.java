package it.terzabin.carmenzon;

import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.util.GenericData;
import com.octo.android.robospice.request.SpiceRequest;

public class EndpointsRequest<T extends GenericData> extends SpiceRequest<T> {
	AbstractGoogleClientRequest<T> operation;

	public EndpointsRequest(AbstractGoogleClientRequest<T> operation) {
		super(operation.getResponseClass());

		this.operation = operation;
	}
	
	public String getURLString() {
		return operation.buildHttpRequestUrl().toString();
	}
	
	public Integer cacheKey() {
		return getURLString().hashCode();
	}
	
	@Override
	public T loadDataFromNetwork() throws Exception {
		return operation.execute();
	}
}