package it.terzabin.carmenzon;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.carmenzon.Carmenzon;
import com.google.api.services.carmenzon.CarmenzonScopes;
import com.octo.android.robospice.SpiceManager;

public abstract class BaseActivity extends Activity {
	private static final String PREF_ACCOUNT_NAME = "accountName";

	private final HttpTransport transport = AndroidHttp.newCompatibleTransport();
	private final JsonFactory jsonFactory = new GsonFactory();
	private GoogleAccountCredential credential;

	protected final SpiceManager serviceManager = new SpiceManager(CarmenzonService.class);
	protected Carmenzon service;
	
	static final int REQUEST_LOGIN = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		List<String> scopes = new ArrayList<String>();
		scopes.add(CarmenzonScopes.USERINFO_EMAIL);
		scopes.add(CarmenzonScopes.USERINFO_PROFILE);
		
		credential = GoogleAccountCredential.usingOAuth2(this, scopes);
		service = new Carmenzon.Builder(transport, jsonFactory, credential).setApplicationName("Carmenzon-Android/1.0").build();
	}

	@Override
	protected void onStart() {
		super.onStart();

		serviceManager.start(this);
	}

	@Override
	protected void onStop() {
		serviceManager.shouldStop();

		super.onStop();
	}

	public SpiceManager getSpiceManager() {
		return serviceManager;
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		checkLogin();
	}
	
	private void checkLogin() {
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		credential.setSelectedAccountName(settings.getString(PREF_ACCOUNT_NAME, null));
		
		if (credential.getSelectedAccountName() == null) {
			requestLogin();
		} else {
			onReady();
		}
	}
	
	private void requestLogin() {
		Intent intent = new Intent(this, LoginActivity.class);
		startActivityForResult(intent, REQUEST_LOGIN);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case REQUEST_LOGIN:
			if (resultCode == Activity.RESULT_OK) {
				checkLogin();
			}
			break;
		}
	}
	
	abstract protected void onReady();
}
