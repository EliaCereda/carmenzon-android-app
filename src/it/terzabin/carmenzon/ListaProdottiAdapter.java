package it.terzabin.carmenzon;

import it.terzabin.carmenzon.ProdottoListItemView.EventListener;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.google.api.services.carmenzon.model.Prodotto;
import com.google.api.services.carmenzon.model.ProdottoCollection;

public class ListaProdottiAdapter extends ArrayAdapter<Prodotto> implements EventListener {

	private Map<Prodotto, Long> ordine;

	public ListaProdottiAdapter(Context context, ProdottoCollection collection) {	
		super(context, 0, collection.getItems());

		azzeraOrdine();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {    
		ProdottoListItemView view;    

		if(convertView != null && convertView instanceof ProdottoListItemView) {
			view = (ProdottoListItemView) convertView;
		} else {
			view = new ProdottoListItemView(getContext(), this);
		}          
		
		Prodotto prodotto = getItem(position);
		view.update(prodotto, ordine.get(prodotto));

		return view;
	}
	
	public void azzeraOrdine() {
		ordine = new HashMap<Prodotto, Long>();
		
		notifyDataSetChanged();
	}
	
	public Map<Prodotto, Long> getOrdine() {
		return ordine;
	}

	@Override
	public void aggiungiProdotto(Prodotto p) {
		Long quantita = ordine.get(p);

		if (quantita == null)
			quantita = 0L;

		quantita++;

		ordine.put(p, quantita);
		
		notifyDataSetChanged();
	}

	@Override
	public void rimuoviProdotto(Prodotto p) {
		Long quantita = ordine.get(p);

		if (quantita != null) {
			quantita--;

			if (quantita <= 0) {
				ordine.remove(p);
			} else {
				ordine.put(p, quantita);
			}
		}
		
		notifyDataSetChanged();
	}
}
