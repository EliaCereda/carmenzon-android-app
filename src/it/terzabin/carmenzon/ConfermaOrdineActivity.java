package it.terzabin.carmenzon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;

import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.carmenzon.model.Ordine;
import com.google.api.services.carmenzon.model.Prodotto;
import com.google.api.services.carmenzon.model.ProdottoOrdinato;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

public class ConfermaOrdineActivity extends BaseActivity implements RequestListener<Ordine> {
	
	public static final String ORDINE = "ordine";
	
	private Map<Prodotto, Long> ordineMap;
	
	private GridView griglia;
	private GrigliaProdottiAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_conferma_ordine);
		// Show the Up button in the action bar.
		setupActionBar();
	}
	
	@Override
	protected void onReady() {
		Intent intent = getIntent();

		@SuppressWarnings("unchecked")
		Map<String, Long> ordineJson = (Map<String, Long>) intent.getExtras().get(ORDINE);
		
		ordineMap = new HashMap<Prodotto, Long>();
		for (Entry<String, Long> entry : ordineJson.entrySet()) {
			String prodottoJson = entry.getKey();
	
			try {
				Prodotto prodotto = new GsonFactory().fromString(prodottoJson, Prodotto.class);
				ordineMap.put(prodotto, entry.getValue());
			} catch (IOException e) {
			}
		}
		
		adapter = new GrigliaProdottiAdapter(this, ordineMap);
		
		griglia = (GridView) findViewById(R.id.griglia);
		griglia.setAdapter(adapter);
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.conferma_ordine, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		
		case R.id.action_conferma:
			Ordine ordine = new Ordine();
			List<ProdottoOrdinato> prodotti = new ArrayList<ProdottoOrdinato>();
			
			for (Entry<Prodotto, Long> entry : ordineMap.entrySet()) {
				ProdottoOrdinato prodotto = new ProdottoOrdinato();
				
				prodotto.setProdotto(entry.getKey().getEntityKey());
				prodotto.setQuantita(entry.getValue());
				
				prodotti.add(prodotto);
			}
			
			ordine.setProdotti(prodotti);
			
			try {
				EndpointsRequest<Ordine> request = new EndpointsRequest<Ordine>(service.ordini().put(ordine));
				serviceManager.execute(request, request.cacheKey(), DurationInMillis.ALWAYS_EXPIRED, this);
			} catch (IOException e) {
				//mostraErrore(e);
			}
			
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}

	@Override
	public void onRequestFailure(SpiceException arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onRequestSuccess(Ordine arg0) {
		// TODO Auto-generated method stub
		
	}

}
