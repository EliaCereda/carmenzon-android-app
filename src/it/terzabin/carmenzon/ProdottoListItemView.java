package it.terzabin.carmenzon;

import java.text.NumberFormat;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.api.services.carmenzon.model.Prodotto;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

public class ProdottoListItemView extends RelativeLayout implements OnClickListener {

	private final static int BOTTONE_AGGIUNGI = 0;
	private final static int BOTTONE_RIMUOVI = 1;

	private Prodotto prodottoCorrente;

	private TextView nomeTextView;
	private TextView descrizioneTextView;
	private TextView prezzoTextView;
	private TextView quantitaTextView;
	private ImageView imageView;

	private EventListener listener;

	public ProdottoListItemView(Context context, EventListener listener) {
		super(context);

		inflateView(context);
		this.listener = listener;
	}
	
	public ProdottoListItemView(Context context) {
		this(context, null);
	}
	
	private void inflateView(Context context) {
		LayoutInflater.from(context).inflate(R.layout.prodotto_list_item, this);

		nomeTextView = (TextView) findViewById(R.id.txtName);
		descrizioneTextView = (TextView) findViewById(R.id.txtDescription);
		prezzoTextView = (TextView) findViewById(R.id.txtPrice);
		quantitaTextView = (TextView) findViewById(R.id.txtQuantity);
		imageView = (ImageView) findViewById(R.id.imgPanino);

		Button aggiungiButton = (Button) findViewById(R.id.btnAdd);
		aggiungiButton.setTag(BOTTONE_AGGIUNGI);
		aggiungiButton.setOnClickListener(this);

		Button rimuoviButton = (Button) findViewById(R.id.btnRemove);
		rimuoviButton.setTag(BOTTONE_RIMUOVI);
		rimuoviButton.setOnClickListener(this);
	}

	public void update(Prodotto prodotto, Long quantita) {
		if (prodottoCorrente != prodotto) {
			prodottoCorrente = prodotto;

			nomeTextView.setText(prodotto.getNome());
			descrizioneTextView.setText(prodotto.getDescrizione());
			
			//Questo arriva da http://stackoverflow.com/a/5033459/1821284
			String prezzo = NumberFormat.getCurrencyInstance(Locale.ITALY).format(prodotto.getPrezzo());
			prezzoTextView.setText(prezzo);
			
			UrlImageViewHelper.setUrlDrawable(imageView, prodotto.getUrlImmagine(), R.drawable.prodotto_default, UrlImageViewHelper.CACHE_DURATION_INFINITE);
		}
		
		if (quantita == null)
			quantita = 0L;
			
		quantitaTextView.setText(quantita.toString());
	}

	@Override
	public void onClick(View v) {
		if (listener != null) {
			switch ((Integer) v.getTag()) {
			case BOTTONE_AGGIUNGI:
				listener.aggiungiProdotto(prodottoCorrente);
				break;
			case BOTTONE_RIMUOVI:
				listener.rimuoviProdotto(prodottoCorrente);
				break;
			}
		}
	}

	public interface EventListener {
		public void aggiungiProdotto(Prodotto p);
		public void rimuoviProdotto(Prodotto p);
	}
}
